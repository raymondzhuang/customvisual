/*
 *  Power BI Visualizations
 *
 *  Copyright (c) Microsoft Corporation
 *  All rights reserved.
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the ""Software""), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

module powerbi.extensibility.visual {
    "use strict";
    import DataViewObjectsParser = powerbi.extensibility.utils.dataview.DataViewObjectsParser;

    export class VisualSettings extends DataViewObjectsParser {
        public textSettings       : textSettings        = new textSettings();
        public squareSettings     : squareSettings      = new squareSettings();
        public shapeSettings      : shapeSettings       = new shapeSettings();
        public showText           : showText            = new showText();
        public kpiStyleSettings   : kpiStyleSettings    = new kpiStyleSettings();
        // public colorSettingsSquare: colorSettingsSquare = new colorSettingsSquare();
        // public colorSettingsArrow : colorSettingsArrow  = new colorSettingsArrow();
        public targetSettings     : targetSettings      = new targetSettings();
    }

    export class textSettings {
     // Text Size
      public responsive: boolean = true;
      public fontSize: number = 12;
      public displayUnits: number = 0;
      public displayUnitsForValue: number = 0;
      public displayUnitsForTarget: number = 0;
      public repPositiveGapAsNegativeNumber: boolean = true;
      public showPercentagesOnGaps: boolean = true;
      public ignoreFormattingForTooltips: boolean = false;  
    }

    export class squareSettings {
        public show        : boolean = true;
        public rounding: boolean = true;
        public roundingSize: number = 5;
        public fillLessThanColor: string = "#ec5858";
        public fillEqualToColor: string = "white";
        public fillGreaterThanColor: string = "#64a871";
        public strokeLessThanColor: string = "#000000";
        public strokeEqualToColor: string = "#000000";
        public strokeGreaterThanColor: string = "#000000";
    }

    export class shapeSettings {
        // public show: boolean = true;
        public style: string = "arrow";
        public fillLessThanColor: string = "red";
        public fillEqualToColor: string = "white";
        public fillGreaterThanColor: string = "green";
        public strokeLessThanColor: string = "#000000";
        public strokeEqualToColor: string = "#000000";
        public strokeGreaterThanColor: string = "#000000";
    }

    export class showText {
        public show: boolean = true;
        public textLessThanColor: string = "#000000";
        public textEqualToColor: string = "#000000";
        public textGreaterThanColor: string = "#000000";
    }

    export class kpiStyleSettings {
        public style: string = "background";
    }

    // export class colorSettingsSquare {
    //   public lessThanColor: string = "#f44336";
    //   public equalToColor: string = "#4caf50";
    //   public greaterThanColor: string = "#4caf50";
    //   public textLessThanColor: string = "#000000";
    //   public textEqualToColor: string = "#000000";
    //   public textGreaterThanColor: string = "#000000";
    //   public targetNotDefinedTextColor: string = "#000000";
    // }

    // export class colorSettingsArrow {
    //   public fillLessThanColor: string = "red";
    //   public fillEqualToColor: string = "white";
    //   public fillGreaterThanColor: string = "green";
    //   public strokeLessThanColor: string = "#000000";
    //   public strokeEqualToColor: string = "#000000";
    //   public strokeGreaterThanColor: string = "#000000";
    // }

    export class targetSettings {
        public showhide:boolean = true;
        public defineTarget: boolean = false;
        public value: number = 0;
    }
}
