/*
 *  Power BI Visual CLI
 *
 *  Copyright (c) Microsoft Corporation
 *  All rights reserved.
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the ""Software""), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

module powerbi.extensibility.visual.simpleKPI8834183003554B1586236E8CAC1ADBE2  {
    "use strict";

    import tooltip = powerbi.extensibility.utils.tooltip;
    import IValueFormatter = powerbi.extensibility.utils.formatting.IValueFormatter;
    import TooltipEventArgs = powerbi.extensibility.utils.tooltip.TooltipEventArgs;
    import ValueFormatter = powerbi.extensibility.utils.formatting.valueFormatter;

    export class SimpleKpiData {
        public value: Field;
        public target: Field;

        public tooltipsData : Field[];

        public constructor() {
            this.tooltipsData = [];
        }

        public gapBetweenValueAndTarget(): Field {
            var ff = new Field(this.target.value - this.value.value, 
                               this.value.format,
                               "Gap - " + this.value.displayName + " & " + this.target.displayName)

            return ff;
        }
    }

    export class SimpleKpiVisualTransform {
        public data: SimpleKpiData;
        public statusMessage: string;
    }
    
    export class Field {
        public value: number;
        public format: string;
        public displayName: string;
        public displayUnits: number;

        public constructor(value: number, format:string, displayName: string, displayUnits? : number) {
            this.value = value;
            this.format = format;
            this.displayName = displayName;
            this.displayUnits = displayUnits ? displayUnits : 0;
        }

        public toString(withFormatting?: boolean, withDisplayUnits?: boolean) {
            var displayUnits = withDisplayUnits ? this.displayUnits : 0;            
            if (withFormatting) {
                return ValueFormatter.create({ format: this.format, value: displayUnits })
                                     .format(this.value)    
            }
            else {
                if (withDisplayUnits) {
                    return ValueFormatter.create({ value: displayUnits })
                                     .format(this.value)    
                } else {    
                    return this.value.toString();
                }                
            }
        }
    }

    /**
     * Function that converts queried data into a view model that will be used by the visual
     *
     * @function
     * @param {VisualUpdateOptions} options - Contains references to the size of the container
     *                                        and the dataView which contains all the data
     *                                        the visual had queried.
     * @param {IVisualHost} host            - Contains references to the host which contains services
     */
    function visualTransform(options: VisualUpdateOptions, host: IVisualHost): SimpleKpiVisualTransform {
        /*Convert dataView to your viewModel*/
        var skvt = new SimpleKpiVisualTransform();
        
        var values = options.dataViews[0].table.rows[0];

        //now set up my data
        //loop through the data set and set up a value mapping table
        var valueArray = []
        valueArray["tooltips"] = [];
        for (var i = 0; i < options.dataViews[0].table.columns.length; i++) {
            var columnRole = options.dataViews[0].table.columns[i].roles;
            if (columnRole["value"] == true) {
                valueArray["value"] = i;
            }
            if (columnRole["target"] == true) {
                valueArray["target"] = i;
            }
            if (columnRole["tooltips"] == true) {
                valueArray["tooltips"].push(i)
            }
        }       

        if (valueArray["value"] == undefined) {
            skvt.data = null;
            skvt.statusMessage = "The value field must be supplied";
            return skvt;
        } 
        
        //collect the data
        var data = new SimpleKpiData();

        var columnsRef = options.dataViews[0].table.columns;

        data.value = new Field(Number(values[valueArray["value"]].toString()),
                            columnsRef[valueArray["value"]].format,
                            columnsRef[valueArray["value"]].displayName);
        
        if (valueArray["target"] != undefined) {
            data.target = new Field(Number(values[valueArray["target"]].toString()),
                                columnsRef[valueArray["target"]].format,
                                columnsRef[valueArray["target"]].displayName); 
        } else {
            data.target = null;
        }

        // now process the tooltips
        for (var i = 0; i < valueArray["tooltips"].length; i++) {
            var toolTipIndex = valueArray["tooltips"][i];
            var tooltipF = new Field(
                Number(values[toolTipIndex].toString()),
                columnsRef[toolTipIndex].format,
                columnsRef[toolTipIndex].displayName,
                0
            );
            data.tooltipsData.push(tooltipF);
        }

        skvt.data = data;
        skvt.statusMessage = null;

        return skvt;
    }

    export class simplekpivisual implements IVisual {
        private settings: VisualSettings;
        private target: HTMLElement;

        private host: IVisualHost;
        private rectangleBackingElement;
        private metricTextElement;
        private arrowElement;

        private svg: d3.Selection<any>;
        private selectionManager : ISelectionManager;
        private tooltipServiceWrapper: tooltip.ITooltipServiceWrapper;

        constructor(options: VisualConstructorOptions) {
            console.log('Visual constructor', options);
            this.target = options.element;
            this.host = options.host;
            this.selectionManager = options.host.createSelectionManager();

            this.tooltipServiceWrapper = tooltip.createTooltipServiceWrapper(
                                                options.host.tooltipService,
                                                options.element);
            
            this.canvas_setup();
        }

        public update(options: VisualUpdateOptions) {

            this.settings =  simplekpivisual.parseSettings(options && options.dataViews && options.dataViews[0]);
            console.log('Visual update', options);
            
            this.canvas_clear();
            
            var transform = visualTransform(options,this.host);

            if (transform.data == null) {
                //print out the error message
            } else {
                var data = transform.data;

                //now if the target has been set to be defined in the settings we need to update it
                if (this.settings.targetSettings.defineTarget) {
                    if (data.target == null) {
                        data.target = new Field(this.settings.targetSettings.value, data.value.format, "Target", 0)
                    } else {
                        data.target.value = this.settings.targetSettings.value;
                    }
                }

                //configure the display units based on the settings
                var tS = this.settings.textSettings;
                data.value.displayUnits = tS.displayUnitsForValue != 0 ? tS.displayUnitsForValue : tS.displayUnits;
                if (data.target != null) {
                    data.target.displayUnits = tS.displayUnitsForTarget != 0 ? tS.displayUnitsForTarget : tS.displayUnits;
                }

                for (var i = 0; i < data.tooltipsData.length; i++) {
                    data.tooltipsData[i].displayUnits = this.settings.textSettings.displayUnits;
                }                

                //we need to derive the backing rectangle colour
                var statusBarColor = this.settings.squareSettings.fillEqualToColor;
                var statusFontColor = this.settings.showText.textEqualToColor;
                var statusStrokeColor = this.settings.squareSettings.strokeEqualToColor;

                if (data.target != null) {
                    if (data.value.value > data.target.value) {
                        statusBarColor = this.settings.squareSettings.fillGreaterThanColor
                        statusFontColor = this.settings.showText.textGreaterThanColor;
                        statusStrokeColor = this.settings.squareSettings.strokeGreaterThanColor;
                    } 
                    else if (data.value.value < data.target.value) {
                        statusBarColor = this.settings.squareSettings.fillLessThanColor;
                        statusFontColor = this.settings.showText.textLessThanColor;
                        statusStrokeColor = this.settings.squareSettings.strokeLessThanColor;
                    }
                } else {
                    // statusFontColor = this.settings.showText.targetNotDefinedTextColor;
                    statusFontColor = this.settings.showText.textEqualToColor;
                }

                //Let's derive some of the sizing
                var svgWidth = parseInt(this.svg.style("width"))
                var svgHeight = parseInt(this.svg.style("height"))

                if (data.target != null && this.settings.squareSettings.show == true) {
                    if (this.settings.squareSettings.rounding == true){
                        this.rectangleBackingElement .append("rect")
                                                    .attr("x", 1)
                                                    .attr("y", 1)
                                                    .attr("rx", this.settings.squareSettings.roundingSize)
                                                    .attr("ry", this.settings.squareSettings.roundingSize)
                                                    .classed("rectBacking",true)
                                                    .attr("stroke-width", 0.5)
                                                    .attr("stroke", statusStrokeColor)
                                                    .attr("width","40%")
                                                    .attr("height", svgHeight - 1)
                                                    .style("fill", statusBarColor);
                    
                    } else { 
                        this.rectangleBackingElement .append("rect")
                                                .attr("x", 1)
                                                .attr("y", 1)
                                                // .attr("rx", this.settings.squareSettings.roundingSize)
                                                // .attr("ry", this.settings.squareSettings.roundingSize)
                                                .classed("rectBacking",true)
                                                .attr("stroke", statusStrokeColor)
                                                .attr("width","40%")
                                                .attr("height", svgHeight - 1)
                                                .style("fill", statusBarColor);
                    }
                    
                } else {
                    this.rectangleBackingElement .append("rect")
                                                .attr("x", 0)
                                                .attr("y", 0)
                                                .attr("rx", 5)
                                                .attr("ry", 5)
                                                .classed("rectBacking",true)
                                                .attr("width","40%")
                                                .attr("height","100%")
                                                .style("fill", "white");
                }




                /*

                    *       *   




                *   *       *   *


                        *
                        x1,y1

                    x2,y2 etc., next point in counter-clockwise order 
                */

                var x1, x2, x3, x4, x5, x6, x7 ;
                var y1, y2, y3, y4, y5, y6, y7 ;

                var rectY = this.rectangleBackingElement.node().getBBox().y;
                var rectX = this.rectangleBackingElement.node().getBBox().x;
                var rectHeight = this.rectangleBackingElement.node().getBBox().height;
                var rectWidth = this.rectangleBackingElement.node().getBBox().width;

                var down = false;
                var fillColor = this.settings.shapeSettings.fillEqualToColor;
                var strokeColor = this.settings.shapeSettings.strokeEqualToColor;
                if (data.target != null) {
                    if (data.value.value > data.target.value) {
                        fillColor = this.settings.shapeSettings.fillGreaterThanColor;
                        strokeColor = this.settings.shapeSettings.strokeGreaterThanColor;
                    } 
                    else if (data.value.value < data.target.value) {
                        down = true;
                        fillColor = this.settings.shapeSettings.fillLessThanColor;
                        strokeColor = this.settings.shapeSettings.strokeLessThanColor;
                    }
                } else {
                }

                if(this.settings.shapeSettings.style == "arrow"){
                    

                    if (down){
                        x1 = 0.5 * rectWidth;
                        y1 =  0.75 * rectHeight;


                        var length = (y1 - 0.7 * y1)/(Math.acos(25*Math.PI/180));
                        var idealLength = 0;
                        if (rectWidth < rectHeight){
                            idealLength = (y1 - 0.7 * y1)/(Math.acos(25*Math.PI/180));
                        } else {
                            idealLength = (y1 - 0.7 * y1)/(Math.acos(25*Math.PI/180));
                        }
                        
                        // console.log("Length is " + String(Math.acos(45*Math.PI/180)));
                        // var length = 1;
                        x2 = Math.max(x1 - length, 0.1 * rectWidth);
                        y2 = 0.75 * y1; 

                        if (rectWidth > rectHeight){
                            x3 = Math.max(0.825 * x1, x1 - (x1 - x2) * 0.45);
                        } else {
                            x3 = Math.min(0.825 * x1, x1 - (x1 - x2) * 0.45);
                        }
                        y3 = y2;

                        x4 = x3;
                        y4 = 0.4 * y1;

                        if (rectWidth > rectHeight){
                            x5 = Math.min(1.175 * x1, x1 + (x1 - x2) * 0.45);
                        } else {
                            x5 = Math.max(1.175 * x1, x1 + (x1 - x2) * 0.45);
                        }
                        y5 = y4;

                        x6 = x5;
                        y6 = y2;

                        x7 = Math.min(x1 + length, 0.9 * rectWidth);
                        y7 = y2;
                    } else {
                        var anchor = 0.9 * rectHeight;
                        x1 = 0.5 * rectWidth;
                        y1 = 0.1 * rectHeight + 0.15 * svgHeight;


                        var length = (anchor - 0.7 * anchor)/(Math.acos(25*Math.PI/180));
                        // console.log("Length is " + String(Math.acos(45*Math.PI/180)));
                        // var length = 1;
                        x2 = Math.max(x1 - length, 0.1 * rectWidth) ;
                        y2 = anchor + y1 - 0.75 * anchor; 

                        if (rectWidth > rectHeight){
                            x3 = Math.max(0.825 * x1, x1 - (x1 - x2) * 0.45);
                        } else {
                            x3 = Math.min(0.825 * x1, x1 - (x1 - x2) * 0.45);
                        }

                        y3 = y2;

                        x4 = x3;
                        y4 = anchor + y1 - 0.4 * anchor;

                        if (rectWidth > rectHeight){
                            x5 = Math.min(1.175 * x1, x1 + (x1 - x2) * 0.45);
                        } else {
                            x5 = Math.max(1.175 * x1, x1 + (x1 - x2) * 0.45);
                        }
                        y5 = y4;

                        x6 = x5;
                        y6 = y2;

                        x7 = Math.min(x1 + length, 0.9 * rectWidth);
                        y7 = y2;
                    }

                    // var lineData = [ { "x": x1,   "y": y1},  { "x": x2,  "y": y2},
                    //   { "x": x3,  "y": y3}, { "x": x4,  "y": y4},
                    //   { "x": x5,  "y": y5}, { "x": x6, "y": y6}, 
                    //   { "x": x7,  "y": y7}, {"x": x1,  "y": y1}];

                    // // var lineData = { "x": 1,   "y": 5};

                    // var lineFunction = d3.svg.line()
                    //          .x(function(d) { return d.x; })
                    //          .y(function(d) { return d.y; })
                    //          .interpolate("linear");


                    this.arrowElement.append("path")
                        .classed("arrowElem",true)
                        // .attr("width", 100)
                        // .attr("height", 100)
                        .attr("stroke-width", 0.5)
                        .attr("stroke", strokeColor)
                        // .attr("d", lineFunction(lineData))
                        .attr("d", "M" + x1 + "," + y1 + "L"+ x2 + "," + y2 + "L" + x3 +","+ y3 + "L" + x4 +","+ y4 + "L" +x5 +","+ y5 + "L" + x5 +","+ y6 +"," + "L" + x7 +","+y7  + "L" + x1 +","+ y1 )
                        .style("fill", fillColor);

                    console.log(this.arrowElement);




                } else {
                    var radius = 0;
                    if (rectWidth < rectHeight){
                        radius = 0.25 * rectWidth;
                    } else {
                        radius = 0.25 * rectHeight;
                    }
                    this.arrowElement.append("circle")
                        .classed("arrowElem",true)
                        .attr("stroke-width", 1)
                        .attr("stroke", strokeColor)
                        .style("fill", fillColor)
                        .attr("cx", rectWidth*0.5 + rectX)
                        .attr("cy", rectHeight*0.5 + rectY)
                        .attr("r", radius);
                }











                
                if (this.settings.kpiStyleSettings.style == "text") {
                    statusFontColor = "black";
                }

                if (this.settings.showText.show == true){

                    this.metricTextElement  .selectAll(".metricText")
                                            .data([data])
                                            .enter()
                                            .append("text")
                                            .classed("metricTxt",true) 
                                            .text(data.value.toString(true, true))
                                            .style("font-family","'Segoe UI', 'wf_segoe-ui_normal', helvetica, arial, sans-serif;")
                                            .style("fill", statusFontColor)
                                            .style("font-size","1px");
                    
                    //now scale the text based on the width / height                
                    var txtHeight = this.metricTextElement.node().getBBox().height;
                    var txtWidth = this.metricTextElement.node().getBBox().width;

                    // if (this.settings.textSettings.responsive == true) {
                    //     var i = 2;
                    //     var textAreaHeight = svgHeight * 0.6
                    //     var textAreaWidth = (svgWidth - rectWidth - rectX);
                    //     //artifically constrain it to do only 19 loops, so maximum is 19em
                    //     while ((txtHeight <= textAreaHeight && txtWidth <= textAreaWidth) && i < 11) {
                    //         this.metricTextElement.selectAll(".metricTxt").style("font-size", i + "em");
                    //         txtHeight = this.metricTextElement.node().getBBox().height;
                    //         txtWidth = this.metricTextElement.node().getBBox().width;
                    //         i++;
                    //     }
                    //     //now if either are greater reduce the text size
                    //     if (txtHeight > textAreaHeight || txtWidth > textAreaWidth) {
                    //         i--;
                    //         this.metricTextElement.selectAll(".metricTxt").style("font-size", i + "em");
                    //         txtHeight = this.metricTextElement.node().getBBox().height;
                    //         txtWidth = this.metricTextElement.node().getBBox().width;
                    //     }
                    //     if (i > 18) {
                    //         this.metricTextElement.selectAll(".metricTxt").style("font-size", "1em");
                    //         txtHeight = this.metricTextElement.node().getBBox().height;
                    //         txtWidth = this.metricTextElement.node().getBBox().width;
                    //     }
                    // } else {
                    //     this.metricTextElement.selectAll(".metricTxt").style("font-size", this.settings.textSettings.fontSize + "px");
                    //     txtHeight = this.metricTextElement.node().getBBox().height;
                    //     txtWidth = this.metricTextElement.node().getBBox().width;
                    // }

                    if (this.settings.textSettings.responsive == true) {
                        var i = 8;
                        var textAreaHeight = svgHeight * 0.6
                        var textAreaWidth = (svgWidth - rectWidth - rectX - 10) * 0.95;
                        //artifically constrain it to do only 19 loops, so maximum is 19em
                        // debugger;
                        while ((txtHeight <= textAreaHeight && txtWidth <= textAreaWidth) && i < 250) {
                            this.metricTextElement.selectAll(".metricTxt").style("font-size", i + "px");
                            txtHeight = this.metricTextElement.node().getBBox().height;
                            txtWidth = this.metricTextElement.node().getBBox().width;
                            if (i <= 48) {
                                i += 1;
                            } else {
                                i += 16;
                            }
                        }
                        //now if either are greater reduce the text size
                        while((txtHeight > textAreaHeight || txtWidth > textAreaWidth) && i > 7) {
                            
                            if (i <= 48){
                                i -= 1;
                            } else {
                                i -= 8;
                            }
                            // console.log("i " + i + " textwidth " + txtWidth + " textAreaWidth " + textAreaWidth + " textHeight " + txtHeight + " textAreaHeight " + textAreaHeight);
                            this.metricTextElement.selectAll(".metricTxt").style("font-size", i + "px");
                            txtHeight = this.metricTextElement.node().getBBox().height;
                            txtWidth = this.metricTextElement.node().getBBox().width;
                        }
                        

                    } else {
                        this.metricTextElement.selectAll(".metricTxt").style("font-size", this.settings.textSettings.fontSize + "px");
                        txtHeight = this.metricTextElement.node().getBBox().height;
                        txtWidth = this.metricTextElement.node().getBBox().width;
                    }
                    
                    var horizontalCenterPoint = svgWidth / 2;
                    var x = horizontalCenterPoint - (txtWidth / 2);

                    var verticalCenterPoint = svgHeight / 2;
                    var y = verticalCenterPoint + (txtHeight / 4);

                    this.metricTextElement.selectAll(".metricTxt").attr("x", x + "px")  
                                                                // .attr("x", ((svgWidth - rectWidth)/2) + rectWidth - (txtWidth/2))
                                                                .attr("x", rectWidth + 10)
                                                                .attr("y", y + "px");

                    this.tooltipServiceWrapper.addTooltip(
                        this.metricTextElement,
                            (tooltipEvent: TooltipEventArgs<number>) => simplekpivisual.getToolTipDataForBar(tooltipEvent.data,this.settings),
                            (tooltipEvent: TooltipEventArgs<number>) => null);  
                }  

            }
        }

        public static getToolTipDataForBar(dataNonCasted: any, settings :VisualSettings) : VisualTooltipDataItem[] {
            var useDisplayUnits = !settings.textSettings.ignoreFormattingForTooltips;
            var data:SimpleKpiData = dataNonCasted;

            if (data != null) {

                var toolTipDataBegin = [data.value];
                if (data.target != null && settings.targetSettings.showhide == true) { toolTipDataBegin.push(data.target); }
                var tooltipDataFieldList = toolTipDataBegin.map(function(f) {
                    return { displayName: f.displayName, value: f.toString(true,useDisplayUnits) }
                })

                var percentageFormatter = ValueFormatter.create({ format: "0.00 %;-0.00 %;0.00 %", value: 1, allowFormatBeautification: true });

                // if (data.target != null && settings.targetSettings.showhide == true) {
                //     var formattedGapValueTarget = "";

                //     var gapTargetField = data.gapBetweenValueAndTarget();
                //     gapTargetField.displayUnits = useDisplayUnits ? settings.textSettings.displayUnits : 0;
                    
                //     gapTargetField.value = settings.textSettings.repPositiveGapAsNegativeNumber == true ? gapTargetField.value * -1 : gapTargetField.value;

                //     formattedGapValueTarget = gapTargetField.toString(true, useDisplayUnits);

                //     if (settings.textSettings.showPercentagesOnGaps == true) {
                //         var formattedPercent = percentageFormatter.format(Math.abs(gapTargetField.value) / data.target.value)
                //         formattedGapValueTarget += "(" + formattedPercent + ")";
                //     }

                //     tooltipDataFieldList.push(
                //         {
                //             displayName: gapTargetField.displayName,
                //             value: formattedGapValueTarget
                //         }
                //     );

                // }

                //now let's push the tooltips
                for (var i = 0; i < data.tooltipsData.length; i++) {
                    var ttData = data.tooltipsData[i];
                    tooltipDataFieldList.push(
                        {
                            displayName: ttData.displayName,
                            value: ttData.toString(true, useDisplayUnits)
                        }
                    )
                }

                //now return the tooltip data
                return tooltipDataFieldList;
            } else {
                return null;
            }
        }

        private canvas_setup() {

            var container = d3.select(this.target)

            this.svg = container.append("svg")
                                .attr("width", "100%")
                                .attr("height", "100%")

            //draw the text
            this.rectangleBackingElement = this.svg.append("g")
                                            .classed("rectangleBacking",true)

            this.metricTextElement = this.svg.append("g")
                                       .classed("metricText",true)

            this.arrowElement = this.svg.append("g").classed("arrowElement", true)
        }

        private canvas_clear() {
            //clear the visual canvas
            this.rectangleBackingElement.selectAll(".rectBacking").remove()
            this.metricTextElement.selectAll(".metricTxt").remove()
            this.arrowElement.selectAll(".arrowElem").remove()
        }

        private static parseSettings(dataView: DataView): VisualSettings {
            return VisualSettings.parse(dataView) as VisualSettings;
        }

        /** 
         * This function gets called for each of the objects defined in the capabilities files and allows you to select which of the 
         * objects and properties you want to expose to the users in the property pane.
         * 
         */
        public enumerateObjectInstances(options: EnumerateVisualObjectInstancesOptions): VisualObjectInstance[] | VisualObjectInstanceEnumerationObject {
            return VisualSettings.enumerateObjectInstances(this.settings || VisualSettings.getDefault(), options);
        }
    }
}